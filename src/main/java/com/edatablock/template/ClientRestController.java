package com.edatablock.template;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.edatablock.dto.InputTemplateDTO;
import com.edatablock.dto.LoginVM;
import com.edatablock.dto.TemplateFieldsDTO;
import com.edatablock.fileupload.StorageService;
import com.edatablock.util.Util;

@RestController
public class ClientRestController {
	
	@Autowired
	private StorageService storageService;

    /*@Autowired
    public ClientRestController(StorageService storageService) {
        this.storageService = storageService;
    }*/
	
	@Value("${ebd.baseurl}")
	private String baseUrl;
	
	@Autowired
	private LoginVM loginVM;

	@RequestMapping(value="/getTemplate/{clientId}", method = RequestMethod.POST)
    public List<InputTemplateDTO> getTemplateList(@PathVariable String clientId){
    	RestTemplate rest =new  RestTemplate(); 
		HttpHeaders header = Util.getAuthenticationHeader(rest, baseUrl, loginVM);
		HttpEntity reqEntity = new HttpEntity<>(header);
		ResponseEntity<List<InputTemplateDTO>> response = rest.exchange(baseUrl+"/input-templates?clientId.equals="+clientId, HttpMethod.GET, reqEntity, new ParameterizedTypeReference<List<InputTemplateDTO>>() {});
		List<InputTemplateDTO> templateList= response.getBody();
    	return templateList;
    }
	
	@RequestMapping(value="/removeTemplateLFields/{id}", method = RequestMethod.POST)
    public void removeTemplateLFields(@PathVariable Integer id){
		ClientTemplateController.fieldLocation.remove(id);
		if(ClientTemplateController.fieldLocation.isEmpty()) {
			ClientTemplateController.id = 0;
		}
    }
	
	@RequestMapping(value="/saveTemplate", method = RequestMethod.POST)
    public ResponseEntity<String> saveTemplate(@RequestBody List<Integer> ids){
		System.out.println(ids);
		ids.forEach(id->{
			TemplateFields templateFields = ClientTemplateController.fieldLocation.get(id);
	    	RestTemplate rest =new  RestTemplate(); 
			HttpHeaders header = Util.getAuthenticationHeader(rest, baseUrl, loginVM);
			
			TemplateFieldsDTO templateFieldsDTO = new TemplateFieldsDTO();
			templateFieldsDTO.setFieldName(templateFields.getFieldName());
			templateFieldsDTO.setFieldZoneMinX(templateFields.getFieldZoneMinX());
			templateFieldsDTO.setFieldZoneMinY(templateFields.getFieldZoneMinY());
			templateFieldsDTO.setFieldZoneMaxX(templateFields.getFieldZoneMaxX());
			templateFieldsDTO.setFieldZoneMaxY(templateFields.getFieldZoneMaxY());
			templateFieldsDTO.setHeight(templateFields.getHeight());
			templateFieldsDTO.setWidth(templateFields.getWidth());
			templateFieldsDTO.setInputTemplateId(new Long(templateFields.getTemplate()));
			
			HttpEntity<TemplateFieldsDTO> reqEntity = new HttpEntity<TemplateFieldsDTO>(templateFieldsDTO, header);
			ResponseEntity<TemplateFieldsDTO> response = rest.exchange(baseUrl+"/template-fields", HttpMethod.POST, reqEntity, TemplateFieldsDTO.class);
			ClientTemplateController.fieldLocation.remove(id);
		});
		if(ClientTemplateController.fieldLocation.isEmpty()) {
			ClientTemplateController.id = 0;
		}
		ResponseEntity<String> res = new ResponseEntity<String>("Data Saved...", HttpStatus.OK);
		return res;
    }
	
	@RequestMapping(value="/removeFile", method = RequestMethod.POST)
    public void removeFile(@RequestParam("file") String file){
		storageService.delete(file);
    }
}
