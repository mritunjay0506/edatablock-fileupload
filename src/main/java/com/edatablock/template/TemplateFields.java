package com.edatablock.template;

import java.io.Serializable;
import java.util.Objects;

/**
 * A TemplateFields.
 */

public class TemplateFields implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private Integer id;

    private String fileName;

    private Double fieldZoneMinX;

    private Double fieldZoneMinY;

    private Double fieldZoneMaxX;

    private Double fieldZoneMaxY;

    private Integer fieldValidationRequire;

    private String fieldValidationRule;
    
    private Integer client;
    
    private Integer template;
    
    private String clientName;
    
    private String templateName;
    
    private String fieldName;

    private Double width;
    
    private Double height;

	public TemplateFields(String fileName, Double fieldZoneMinX, Double fieldZoneMinY, Double fieldZoneMaxX,
			Double fieldZoneMaxY, Integer client,
			Integer template, String clientName, String templateName, String fieldName, Double width, Double height) {
		super();
		this.fileName = fileName;
		this.fieldZoneMinX = fieldZoneMinX;
		this.fieldZoneMinY = fieldZoneMinY;
		this.fieldZoneMaxX = fieldZoneMaxX;
		this.fieldZoneMaxY = fieldZoneMaxY;
		this.client = client;
		this.template = template;
		this.clientName = clientName;
		this.templateName = templateName;
		this.fieldName = fieldName;
		this.width = width;
		this.height = height;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Double getFieldZoneMinX() {
		return fieldZoneMinX;
	}

	public void setFieldZoneMinX(Double fieldZoneMinX) {
		this.fieldZoneMinX = fieldZoneMinX;
	}

	public Double getFieldZoneMinY() {
		return fieldZoneMinY;
	}

	public void setFieldZoneMinY(Double fieldZoneMinY) {
		this.fieldZoneMinY = fieldZoneMinY;
	}

	public Double getFieldZoneMaxX() {
		return fieldZoneMaxX;
	}

	public void setFieldZoneMaxX(Double fieldZoneMaxX) {
		this.fieldZoneMaxX = fieldZoneMaxX;
	}

	public Double getFieldZoneMaxY() {
		return fieldZoneMaxY;
	}

	public void setFieldZoneMaxY(Double fieldZoneMaxY) {
		this.fieldZoneMaxY = fieldZoneMaxY;
	}

	public Integer getFieldValidationRequire() {
		return fieldValidationRequire;
	}

	public void setFieldValidationRequire(Integer fieldValidationRequire) {
		this.fieldValidationRequire = fieldValidationRequire;
	}

	public String getFieldValidationRule() {
		return fieldValidationRule;
	}

	public void setFieldValidationRule(String fieldValidationRule) {
		this.fieldValidationRule = fieldValidationRule;
	}

	public Integer getClient() {
		return client;
	}

	public void setClient(Integer client) {
		this.client = client;
	}

	public Integer getTemplate() {
		return template;
	}

	public void setTemplate(Integer template) {
		this.template = template;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getTemplateName() {
		return templateName;
	}

	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Double getWidth() {
		return width;
	}

	public void setWidth(Double width) {
		this.width = width;
	}

	public Double getHeight() {
		return height;
	}

	public void setHeight(Double height) {
		this.height = height;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((client == null) ? 0 : client.hashCode());
		result = prime * result + ((fieldValidationRequire == null) ? 0 : fieldValidationRequire.hashCode());
		result = prime * result + ((fieldValidationRule == null) ? 0 : fieldValidationRule.hashCode());
		result = prime * result + ((fieldZoneMaxX == null) ? 0 : fieldZoneMaxX.hashCode());
		result = prime * result + ((fieldZoneMaxY == null) ? 0 : fieldZoneMaxY.hashCode());
		result = prime * result + ((fieldZoneMinX == null) ? 0 : fieldZoneMinX.hashCode());
		result = prime * result + ((fieldZoneMinY == null) ? 0 : fieldZoneMinY.hashCode());
		result = prime * result + ((fileName == null) ? 0 : fileName.hashCode());
		result = prime * result + ((template == null) ? 0 : template.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TemplateFields other = (TemplateFields) obj;
		if (client == null) {
			if (other.client != null)
				return false;
		} else if (!client.equals(other.client))
			return false;
		if (fieldValidationRequire == null) {
			if (other.fieldValidationRequire != null)
				return false;
		} else if (!fieldValidationRequire.equals(other.fieldValidationRequire))
			return false;
		if (fieldValidationRule == null) {
			if (other.fieldValidationRule != null)
				return false;
		} else if (!fieldValidationRule.equals(other.fieldValidationRule))
			return false;
		if (fieldZoneMaxX == null) {
			if (other.fieldZoneMaxX != null)
				return false;
		} else if (!fieldZoneMaxX.equals(other.fieldZoneMaxX))
			return false;
		if (fieldZoneMaxY == null) {
			if (other.fieldZoneMaxY != null)
				return false;
		} else if (!fieldZoneMaxY.equals(other.fieldZoneMaxY))
			return false;
		if (fieldZoneMinX == null) {
			if (other.fieldZoneMinX != null)
				return false;
		} else if (!fieldZoneMinX.equals(other.fieldZoneMinX))
			return false;
		if (fieldZoneMinY == null) {
			if (other.fieldZoneMinY != null)
				return false;
		} else if (!fieldZoneMinY.equals(other.fieldZoneMinY))
			return false;
		if (fileName == null) {
			if (other.fileName != null)
				return false;
		} else if (!fileName.equals(other.fileName))
			return false;
		if (template == null) {
			if (other.template != null)
				return false;
		} else if (!template.equals(other.template))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TemplateFields [fileName=" + fileName + ", fieldZoneMinX=" + fieldZoneMinX + ", fieldZoneMinY="
				+ fieldZoneMinY + ", fieldZoneMaxX=" + fieldZoneMaxX + ", fieldZoneMaxY=" + fieldZoneMaxY
				+ ", fieldValidationRequire=" + fieldValidationRequire + ", fieldValidationRule=" + fieldValidationRule
				+ ", client=" + client + ", template=" + template + "]";
	}

    
}
