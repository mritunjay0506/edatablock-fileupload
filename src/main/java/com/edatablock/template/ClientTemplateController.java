package com.edatablock.template;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import com.edatablock.dto.ClientDTO;
import com.edatablock.dto.LoginVM;
import com.edatablock.util.Util;

@Controller
public class ClientTemplateController {

    public static Map<Integer, TemplateFields> fieldLocation = new HashMap<Integer, TemplateFields>();
    
    public static Integer id = 0;
    
    private List<ClientDTO> clientList;
    
    @Value("${ebd.baseurl}")
	private String baseUrl;
	
	@Autowired
	private LoginVM loginVM;

    static {
        //fieldLocation.add(new TemplateFields("Invoice",163.0,122.0,12.0,12.0));
    }

    // Inject via application.properties
    @Value("${welcome.message}")
    private String message;

    @Value("${error.message}")
    private String errorMessage;

    @RequestMapping(value = {"/static/template/images", "/index" }, method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("message", message);
        return "index";
    }

    @RequestMapping(value = {"/static/template/images","/list"}, method = RequestMethod.GET)
    public String fieldList(Model model) {
        model.addAttribute("selectionList", fieldLocation);
        return "list";
    }

    @RequestMapping(value = {"/static/template/images","addTemplate" }, method = RequestMethod.GET )
    public String showAddTemplateFieldPage(Model model,@RequestParam("fileName") String fileName) {
        System.out.println("filename "+fileName);
        TemplateForm templateForm = new TemplateForm();
        templateForm.setFileName(fileName);
        if(clientList==null) {
        	clientList = getClientList();
        }
        templateForm.setClientList(clientList);
        model.addAttribute("templateForm", templateForm);
        
        return "addTemplate";
    }

    @RequestMapping(value = {"/static/template/images","/addTemplate" }, method = RequestMethod.POST)
    public String saveTemplateFields(Model model, //
                             @ModelAttribute("templateForm") TemplateForm templateForm) {

        //Integer id = templateForm.getId();
    	Double maxX = templateForm.getFieldZoneMaxX();
        Double maxY = templateForm.getFieldZoneMaxY();
        Double minX = templateForm.getFieldZoneMinX();
        Double minY = templateForm.getFieldZoneMinY();
        String fileName = templateForm.getFileName();
        Integer client = templateForm.getClient();
        Integer template = templateForm.getTemplate();
        String clientName = templateForm.getClientName();
        String templateName = templateForm.getTemplateName();
        String fieldName = templateForm.getFieldName();
        Double width = templateForm.getWidth();
        Double height = templateForm.getHeight();

        if (maxX != null   //
                && maxY != null ) {
			TemplateFields templateFields = new TemplateFields(fileName, minX, minY, maxX, maxY, client, template,
					clientName, templateName, fieldName, width, height);
            fieldLocation.put(++id,templateFields);

            return "redirect:/list";
        }
        templateForm.setClientList(clientList);
        model.addAttribute("errorMessage", errorMessage);
        return "addTemplate";
    }
    
    /*@RequestMapping(value="/getTemplate/{clientId}", method = RequestMethod.POST)
    public List<InputTemplateDTO> getTemplateList(@PathVariable String clientId){
    	RestTemplate rest =new  RestTemplate(); 
		HttpHeaders header = Util.getAuthenticationHeader(rest, baseUrl, loginVM);
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity reqEntity = new HttpEntity<>(header);
		ResponseEntity<List<InputTemplateDTO>> response = rest.exchange(baseUrl+"/input-templates?clientId.equals="+clientId, HttpMethod.GET, reqEntity, new ParameterizedTypeReference<List<InputTemplateDTO>>() {});
		List<InputTemplateDTO> templateList= response.getBody();
    	return templateList;
    }*/
    
    private List<ClientDTO> getClientList() {
		RestTemplate rest =new  RestTemplate(); 
		HttpHeaders header = Util.getAuthenticationHeader(rest, baseUrl, loginVM);
		HttpEntity reqEntity = new HttpEntity<>(header);
		ResponseEntity<List<ClientDTO>> response = rest.exchange(baseUrl+"/clients", HttpMethod.GET, reqEntity, new ParameterizedTypeReference<List<ClientDTO>>() {});
		List<ClientDTO> clientList= response.getBody();
		return clientList;
	}
}
